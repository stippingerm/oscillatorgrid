# oscillatorgrid

# README #

Python implementation of a simple **Oscillator Grid**.
Each neighbor modifies its phase relative to the central $C_{i,j}$ by
decreasing the phase difference with an amount proportional to the absolute phase difference.


#### git setup


```
git config --global user.name "user-name"

git config --global user.email "user-email"

git clone https://gitlab.com/stippingerm/oscillatorgrid.git

```

#### Installation


##### linux (ubuntu 18.04)

The package can be installed with the pip package manager. After opening up a terminal window and entering to the folder,
one can install **oscillatorgrid** in editable mode (for developers):

```
pip install -e . 
```


#### Running without installation

Note: for running without installation you need a recent version of the following packages installed manually:
* `numpy`,
* `scipy`,
* `matplotlib`,
* `scikit-learn`.


The package can be run from command line:

```
python -m oscillatorgrid <parameters>
```

for help run

```
python -m oscillatorgrid -h
```

Alternatively, you can use the attached Jupyter notebook.
