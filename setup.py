from distutils.core import setup

setup(
    name='oscillatorgrid',
    version='beta',
    author='Marcell Stippinger, Wigner RCP, <stippinger.marcell@wigner.hu>',
    author_email='stippingerm@gmail.com',
    packages=["oscillatorgrid"],
    url='https://gitlab.com/stippingerm/oscillator-grid',
    license='LICENSE.txt',
    description='Simple Oscillator Grid Simulation',
    long_description=open('README.md').read(),
    install_requires=[
        "numpy",
        "scipy",
        "matplotlib",
        "scikit-learn",
    ],
)
