import argparse
import numpy as np

from .grid import Grid, simulate
from .figures import animation1, figure2, figure3, figure4, plt


def _make_argparser():
    p = argparse.ArgumentParser(allow_abbrev=False)

    g1 = p.add_argument_group(title='io')
    group = g1.add_mutually_exclusive_group()
    group.add_argument('--show', action='store_true', help='Show results in an interactive manner')
    group.add_argument('--noshow', dest='show', action='store_false',
                       help='Just do the calculation without opening graphical window')

    g2 = p.add_argument_group(title='parameters')
    g2.add_argument('-m', '--grid_length', type=int, default=8,
                    help='Side length of the oscillator grid')
    g2.add_argument('-f', '--frequency', type=float, default=10.,
                    help='Frequency of the oscillator grid')
    g2.add_argument('-lr', '--learning_rate', type=float, default=0.01,
                    help='Learning rate that influences how fast oscillators adjust phase')
    g2.add_argument('-dt', '--timestep', type=float, default=0.002,
                    help='Time step of the simulation')
    g2.add_argument('-n', '--simulation_steps', type=int, default=400,
                    help='Number of time steps in the simulation')
    g2.add_argument('-s', '--schedule', choices=('parallel', 'permutation', 'random'), type=str, default='parallel',
                    help='Schedule for updating phase with respect to neighbors.')
    g2.add_argument('-r', '--seed', type=int, default=137,
                    help='Random seed')

    p.set_defaults(show=True)
    return p


if __name__ == '__main__':
    parser = _make_argparser()
    args = parser.parse_args()
    my_grid = Grid(m=args.grid_length,
                   frequency=args.frequency,
                   learning_rate=args.learning_rate)
    result = simulate(my_grid,
                      dt=args.timestep,
                      n=args.simulation_steps,
                      schedule=args.schedule,
                      random_state=args.seed)
    id1, id2 = np.random.choice(my_grid.size, size=2, replace=False)
    # Note: we must hold the reference anim to keep the animation going
    # To plot in a single figure, uncomment:
    # fig, ax = plt.subplots(2, 2, figsize=(10, 10))
    anim = animation1(result)  # ax=ax[0, 0])
    figure2(result, unwrap=False)  # ax=ax[0, 1])0
    # ax[1, 0].remove()
    figure3(result, id=id1, tau=20)  # fig=fig, axis_args=(2,2,3))
    figure4(result, id1=id1, id2=id2)  # ax=ax[1, 1])
    if args.show:
        plt.show()
