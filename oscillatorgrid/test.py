from oscillatorgrid.grid import Grid, ComplexOscillator, FloatOscillator, PhaseInteraction, simulate
from unittest import TestCase
import numpy as np


class TestGrid(TestCase):
    def test_grid(self):
        my_grid = Grid(m=16,  # grid_length,
                       frequency=10,
                       learning_rate=0.01)
        my_grid.initialize_cells()
        return my_grid

    def test_complex(self):
        my_grid = ComplexOscillator([3, 3])
        my_grid.amplitude = 1
        my_grid.phase = np.array([[0, 1, 2], [3, 4, 5], [6, 7, 8]]) * np.pi / 2
        return my_grid

    def test_permutation(self):
        my_grid = self.test_grid()
        result = simulate(my_grid,
                          dt=0.002,
                          n=50,
                          schedule='permutation',
                          random_state=317)

    def test_parallel(self):
        my_grid = self.test_grid()
        result = simulate(my_grid,
                          dt=0.002,
                          n=50,
                          schedule='parallel',
                          random_state=317)

    def test_interaction(self):
        interaction = PhaseInteraction(potential=1, symmetry=1, representation='float')
        updated = interaction(np.array([-3, -2, -1, 0, 1, 2, 3, 4]), np.array([1.5]), 0.1)
        pass

    def test_gridinteraction(self):
        my_grid = Grid(m=16,  # grid_length,
                       frequency=10,
                       learning_rate=0.01,
                       interaction=PhaseInteraction(potential=1, symmetry=2, representation='float'))
        result = simulate(my_grid,
                          dt=0.002,
                          n=50,
                          schedule='permutation',
                          random_state=317)
        pass

    def test_neighbors(self):
        my_grid = self.test_grid()
        n1 = my_grid.neighbors_of([3, 3], exclude_self=True)
        n2 = my_grid.neighbors_of([3, 3], exclude_self=False)
        n3 = my_grid.neighbors_of([(3, 3), (4, 4)], exclude_self=True)
        n4 = my_grid.neighbors_of([(3, 3), (4, 4)], exclude_self=False)
        pass
