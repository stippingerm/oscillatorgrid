import numpy as np
from matplotlib import pyplot as plt
from matplotlib import animation, ticker
from matplotlib.collections import LineCollection
from mpl_toolkits.mplot3d import Axes3D

cmap_name = 'jet'


def _establish_axes(ax, *args, **kwargs):
    """
    Find the figure and axis corresponding to `ax` or create one

    Parameters
    ----------
    ax: Optional[plt.Axes]
        if None provided, create one with `args` and `kwargs` passed
    *args, **kwargs:
        parameters for creating axes

    Returns
    -------
    fig: plt.Figure
        figure
    ax: plt.Axes
        axes
    """
    if ax is None:
        fig, ax = plt.subplots(*args, **kwargs)
    else:
        fig = np.ravel(ax)[0].figure
    return fig, ax


def _rad_ticks(axis):
    axis.set_ticks([-np.pi, -0.5 * np.pi, 0, 0.5 * np.pi, np.pi])
    axis.set_ticklabels([r'$-\pi$', r'$-\pi/2$', r'$0$', r'$\pi/2$', r'$\pi$'])


def _id_to_string(result, id):
    coordinates = result['coordinates']
    txt = '$C_{{{},{}}}$'.format(*coordinates[id])
    return txt


def _disconnect(x, threshold=np.pi, axis=0):
    broken = np.abs(np.diff(x, axis=axis)) > threshold
    broken = np.insert(broken, 0, False, axis=axis)
    result = np.ma.masked_where(broken, x)
    return result


def figure1(result, t=0, ax=None):
    """
    Visualize the oscillator cells

    Parameters
    ----------
    result: dict
        from the simulation
    t: int
        time step
    ax: plt.Axis
        where to plot

    Returns
    -------
    fig: plt.Figure
        figure
    ax: plt.Axis
        axis
    txt: plt.Text
        field to display frame count
    quiver: plt.Quiver
        quiver plot of the matrix
    """
    fig, ax = _establish_axes(ax)  # type: plt.Figure, plt.Axes
    ax.axis('equal')
    ax.set_xlabel('i')
    ax.set_ylabel('j')
    ax.set_title('Oscillator cells $C_{i,j}$')
    ax.get_xaxis().get_major_locator().set_params(integer=True)
    ax.get_yaxis().get_major_locator().set_params(integer=True)

    txt = ax.text(0.99, 0.99, 'time', horizontalalignment='right', verticalalignment='top', transform=ax.transAxes)
    try:
        x, y = result['coordinates'][:, 0], result['coordinates'][:, 1]
    except IndexError:
        x, y = result['coordinates'][:, 0], 0  # support for 1D grid
    n = len(x)
    u, v = np.cos(result['phases'][t]), np.sin(result['phases'][t])
    quiver = ax.quiver(x, y, u, v, np.arange(n), cmap=cmap_name, units='xy', angles='uv', pivot='middle')
    cbar = plt.colorbar(quiver, ax=ax)
    cbar.ax.set_ylabel('Cell ID')
    return fig, ax, txt, quiver


def update1(data, txt, quiver):
    """
    Visualize the oscillator cells

    Parameters
    ----------
    data: tuple(int, np.ndarray)
        frame data to be displayed
    txt: plt.Text
        field to display frame count to be updated
    quiver: plt.Quiver
        quiver plot of the matrix to be updated

    Returns
    -------
    txt: plt.Text
        field to display frame count
    quiver: plt.Quiver
        quiver plot of the matrix
    """
    i, phases = data
    txt.set_text('Step {}'.format(i))
    u, v = np.cos(phases), np.sin(phases)
    quiver.set_UVC(u, v)
    return txt, quiver


def animation1(result, ax=None):
    """
    Visualize the oscillator cells as an automated animation

    Parameters
    ----------
    result: dict
        from the simulation
    ax: plt.Axis
        where to plot

    Returns
    -------
    anim: animation.FuncAnimation
        this reference must be kept in scope to keep the animation running,
        i.e., store the return value in a persistent variable
    """
    fig, ax, txt, quiver = figure1(result, ax=ax)
    frames = list(enumerate(result['phases']))
    anim = animation.FuncAnimation(fig, update1, frames=frames, fargs=(txt, quiver),
                                   interval=20, repeat_delay=1000, blit=False)
    return anim


def figure2(result, offset=0, unwrap=True, lw=1.5, ax=None):
    """
    Visualize the oscillator phases as a function of time

    Parameters
    ----------
    result: dict
        from the simulation
    offset: float
        vertical offset between plots of cells
    unwrap: bool
        unwrap phase to be continuous (very useful when frequency==0)
    lw: float
        line width (this needs to be adjusted across matplotlib backends to have a nice fill ratio)
    ax: plt.Axis
        where to plot

    Returns
    -------
    fig: plt.Figure
        figure
    ax: plt.Axis
        axis
    lines: plt.LineCollection
        the lines
    """
    fig, ax = _establish_axes(ax)  # type: plt.Figure, plt.Axes
    ax.set_xlabel('time [s]')
    ax.set_ylabel('phases [rad]')
    ax.set_title('Time course')
    _rad_ticks(ax.get_yaxis())

    time = result['time']
    phases = result['phases']
    n = phases.shape[1]
    if unwrap:
        initial = phases[[0]]
        changes = np.unwrap(np.diff(phases, axis=0), axis=0)
        phases = np.cumsum(np.concatenate((initial, changes), axis=0), axis=0)
    else:
        phases = phases + offset * np.arange(n)
    segs = [_disconnect(np.column_stack((time, y)), axis=0) for y in phases.T]
    lines = LineCollection(segs, alpha=0.5, linewidths=lw, cmap=cmap_name, linestyle='solid')
    lines.set_array(np.arange(n))
    ax.add_collection(lines)
    ax.set_ylim(-np.pi, offset * (n - 1) + np.pi)
    return fig, ax, lines


def _data3(result, id, tau):
    """Extract data for `figure3`, see variable description there"""
    phases = result['phases']
    try:
        timeseries = phases[:, id]
    except IndexError:
        raise IndexError('Invalid cell id was provided for recurrence plot')
    try:
        x = timeseries[0:-2 * tau]
        y = timeseries[1 * tau:-1 * tau]
        z = timeseries[2 * tau:None]
    except IndexError:
        raise IndexError('Not enough simulation steps for recurrence plot')
    t = len(x)
    return t, x, y, z


def figure3(result, id=3, tau=1, fig=None, axis_args=(), **kwargs):
    """
    Visualize the return diagram

    Parameters
    ----------
    result: dict
        from the simulation
    id: int
        cell, 0<=id<grid.size
    tau: int
        time step
    axis_args: tuple
        arguments for axis creation
    kwargs: dict
        keyword arguments for axis creation

    Returns
    -------
    fig: plt.Figure
        figure
    ax: plt.Axis
        axis
    sc: plt.Scatter
        scatter plot
    """
    if fig is None:
        fig = plt.figure()
    ax = fig.add_subplot(*axis_args, projection='3d', **kwargs)  # type: plt.Axes3D
    ax.set_xlabel(r'$C_{i,j}(t-2\tau)$')
    ax.set_ylabel(r'$C_{i,j}(t-\tau)$')
    ax.set_zlabel(r'$C_{i,j}(t)$')
    coordinates = result['coordinates']
    ax.set_title(r'Recurrence plot for {} with $\tau={}$'.format(_id_to_string(result, id), tau))
    _rad_ticks(ax.get_xaxis())
    _rad_ticks(ax.get_yaxis())
    _rad_ticks(ax.get_zaxis())
    ax.set_xlim(-np.pi, np.pi)
    ax.set_ylim(-np.pi, np.pi)
    ax.set_zlim(-np.pi, np.pi)

    t, x, y, z = _data3(result, id, tau)
    sc = ax.scatter(x, y, z, c=np.arange(t), cmap='viridis', marker='o')
    cbar = plt.colorbar(sc, ax=ax)
    cbar.ax.set_ylabel('Timestep')
    return fig, ax, sc


def update3(sc, result, id, tau):
    """Update data for `figure3`, see variable description there"""
    # Note: 3D plots are sometimes difficult to update, one needs to hack into internals and schedule redraw manually
    t, x, y, z = _data3(result, id, tau)
    sc._offsets3d = np.c_[x, y, z]


def _data4(result, id1, id2):
    """Extract data for `figure4`, see variable description there"""
    phases = result['phases']
    try:
        timeseries1 = phases[:, id1]
        timeseries2 = phases[:, id2]
    except IndexError:
        raise IndexError('Invalid cell id was provided for recurrence plot')
    t = len(timeseries1)
    return t, timeseries1, timeseries2


def figure4(result, id1=13, id2=25, ax=None):
    """
    Visualize the correlatedness of two oscillators

    Parameters
    ----------
    result: dict
        from the simulation
    id1, id2: int
        cells, 0<=id<grid.size
    ax: plt.Axis
        where to plot

    Returns
    -------
    fig: plt.Figure
        figure
    ax: plt.Axis
        axis
    sc: plt.Scatter
        scatter plot
    """
    fig, ax = _establish_axes(ax)  # type: plt.Figure, plt.Axes
    ax.axis('equal')
    ax.set_xlabel(_id_to_string(result, id1))
    ax.set_ylabel(_id_to_string(result, id2))
    ax.set_title('Correlation plot')
    _rad_ticks(ax.get_xaxis())
    _rad_ticks(ax.get_yaxis())
    ax.set_xlim(-np.pi, np.pi)
    ax.set_ylim(-np.pi, np.pi)

    t, timeseries1, timeseries2 = _data4(result, id1, id2)
    ax.plot([-4, 4], [-4, 4], color='tab:red', linestyle='--')
    sc = ax.scatter(timeseries1, timeseries2, c=np.arange(t), cmap='viridis', marker='o')
    return fig, ax, sc


def update4(sc, result, id1, id2):
    """Update data for `figure4`, see variable description there"""
    t, timeseries1, timeseries2 = _data4(result, id1, id2)
    sc.set_offsets(np.c_[timeseries1, timeseries2])
    sc.axes.set_xlabel(_id_to_string(result, id1))
    sc.axes.set_ylabel(_id_to_string(result, id2))
