import numpy as np


def to_complex(amplitude, phase):
    """Create complex number from amplitude and phase"""
    # make sure that real numbers are received
    amplitude = np.asarray(amplitude, dtype=float)
    phase = np.asarray(phase, dtype=float)
    # do conversion
    return amplitude * np.exp(1j * phase)


def to_unit_circle(cplx):
    """Project complex numbers to unit circle (unit amplitude, phase preserved)"""
    result = cplx / np.abs(cplx)
    return result
