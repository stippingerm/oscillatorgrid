import numpy as np
from .arithmetic import to_complex, to_unit_circle


# Oscillators have phase (phi) and amplitude (a) encoded as a complex number (grid=a + e^{j * phi})
# in addition they have a frequency stored as a float (freq)
#
# Updates may be described by the force acting on the oscillator (the force may come from a potential):
#
#   U = - beta * cos(phi1-phi2) + c * |a1-a2| + d * |f1-f2|
#   -F ~ dU = beta * sin(phi1-phi2), c * sgn(a1-a2), d * sgn(f1-f2)
#
#   where cos and sin may be replaced with abs and sgn respectively
#
# or carried out for a finite time step:
#
#   Delta = to_unit_circle((1-lr) * e^{j * phi1} + lr * e^{j * phi2}) - e^{j * phi1}, lr * (a2 - a1), lr * (f2 - f1)
#
#   where normalization to unit circle and referring it to original value ensures that phase vector remains unit length,
#   alternatively
#
#   Delta phi = lr * [(phi2 - phi1) mod (2 * pi) projected on (-pi, pi)]
#
# Note: when you aggregate multiple updates then it is easy to show the correctness of the vectorial solution while
# you have to do the modulo stuff before aggregating in the alternative solution.
#
# Zoltan Nadasdy suggested a different potential that introduces attraction between phases close to each other and
# repulsion between phases farther than pi/2, such a potential would be
#
#   U = - beta * cos(2*(phi1-phi2)) + c * |a1-a2| + d * |f1-f2|
#   -F ~ dU = 2 * beta * sin(2*(phi1-phi2)), c * sgn(a1-a2), d * sgn(f1-f2)
#   where replacing cos requires a M shaped function and sin becomes a step function Theta[x-pi/2] mod pi
#
# or carried out for a finite time step:
#
#   Delta phi = lr * [(phi2 - phi1) mod (pi) projected on (-pi/2, pi/2)]
#
# unfortunately, the vector solution seems difficult.
#


def np_wrap_around(value, boundaries=(-np.pi, np.pi)):
    """

    Parameters
    ----------
    value: array_like
        phase of the oscillator
    boundaries: list|array_like
        left-closed right-open interval into which values are projected

    Returns
    -------
    difference: array_like
        phase difference, values within boundaries

    >>> from oscillatorgrid.evolution import np_wrap_around
    >>> np_phase_diff([-1, 0, 1, 2, 3, 4], [-2, 2])
    array([-1,  0,  1, -2, -1,  0])
    """
    boundaries = np.asarray(boundaries)
    loop = np.diff(boundaries, axis=-1)
    wrapped = np.mod(value, loop)
    shift = np.where(boundaries[..., 1] <= wrapped, loop, 0)
    return wrapped - shift


def np_phase_diff(phase, reference, boundaries=(-np.pi, np.pi)):
    """

    Parameters
    ----------
    phase: array_like
        phase of the oscillator
    reference: numeric|array_like
        reference phase
    boundaries: list|array_like
        left-closed right-open interval into which values are projected

    Returns
    -------
    difference: array_like
        phase difference, values within boundaries

    >>> from oscillatorgrid.evolution import np_phase_diff
    >>> np_phase_diff([1, 2, 3, 4, 5, 6], 2, [-2, 2])
    array([-1,  0,  1, -2, -1,  0])
    """
    boundaries = np.asarray(boundaries)
    loop = np.diff(boundaries, axis=-1)
    diff = np.subtract(phase, reference)
    wrapped = np.mod(diff, loop)
    shift = np.where(boundaries[..., 1] <= wrapped, loop, 0)
    return wrapped - shift


def cplx_phase_attraction(original, preferred, learning_rate):
    """
    Update based on force proportional to difference
    Note: amplitude may drift due to roundoff errors
    """
    return to_complex(np.abs(original), np.angle((1 - learning_rate) * original + learning_rate * preferred))


def circ_phase_attraction(original, preferred, learning_rate, boundaries=(-np.pi, np.pi)):
    """
    Phase update based on force proportional to difference
    Note: you can choose the symmetry applied to the force by properly setting boundaries; boundaries do not affect the
    range of phases the oscillators are allowed to take and this latter is assumed to be in [-pi, pi)
    """
    amplitude, original, preferred = np.abs(original), np.angle(original), np.angle(preferred)
    # wrap around to let each force act individually with the correct sign
    # Note: you can apply sgn before mean to impose a V-shaped potential instead of the current (quadratic) while
    # using the potential implied by the scalar product requires further work.
    force = np.mean(np_wrap_around(preferred - original, boundaries=boundaries), axis=-1)
    # wrap around to have the result in the valid range
    phase = np_wrap_around(original + force * learning_rate)
    return to_complex(amplitude, phase)


class PhaseInteraction(object):
    def __init__(self, potential, symmetry, representation):
        """
        Class that implements typical interactions

        Parameters
        ----------
        potential: str|int
            * 'cos' based on scalar product
            * 1 - V-shaped potential resulting in constant-length force vector
            * 2 - Quadratic potential resulting in force linear n the distance
        symmetry: int
            1-fold symmetry uses conventional attraction-repulsion while
            2-fold symmetry allows parallel and anti-parallel alignment
        representation: str
            'float' - store phase and amplitude separately
            'complex' - store phase and amplitude as a complex number
        """
        self.potential = potential
        self.symmetry = symmetry
        self.representation = representation
        self.check_params()

    def check_params(self):
        if self.potential not in ('cos', 1, 2):
            raise ValueError
        if int(self.symmetry) != self.symmetry:
            raise ValueError
        if self.representation not in ('float', 'complex'):
            raise ValueError

    def __call__(self, original, preferred, learning_rate, axis=(), discard_nan=False):
        if self.representation == 'complex':
            raise NotImplementedError
        boundaries = np.array([-np.pi, np.pi]) / self.symmetry
        # wrap around to let each force act individually with the correct sign
        # Note: you can apply sgn before mean to impose a V-shaped potential instead of the current (quadratic) while
        # using the potential implied by the scalar product requires further work.
        forces = np_wrap_around(preferred - original, boundaries=boundaries)
        if discard_nan:
            forces = np.nan_to_num(forces)
        if self.potential == 1:
            forces = np.sign(forces)
        elif self.potential == 'cos':
            raise NotImplementedError
        # wrap around to have the result in the valid range
        phase = np_wrap_around(original + np.mean(forces, axis=axis, keepdims=True) * learning_rate)
        return phase
