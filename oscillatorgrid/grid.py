import numpy as np
from sklearn.utils import check_random_state

# Note: For simplicity, we do no strict input validation but parameters are sometimes cast to the expected type

# Note: representing phase is complex number is advantageous to handle the periodic boundary in the phase value,
# i.e., -pi and pi are the same but usual distance measures do not recognize that
from oscillatorgrid.arithmetic import to_complex, to_unit_circle
from oscillatorgrid.evolution import cplx_phase_attraction, circ_phase_attraction, PhaseInteraction
from abc import abstractproperty, abstractmethod, ABC


class OscillatorBase(ABC):
    @property
    @abstractmethod
    def phase(self):
        """
        Get the phase of all cells

        Returns
        -------
        phases: np.ndarray[float]
            phases in range [-pi, pi]
        """
        pass

    @phase.setter
    @abstractmethod
    def phase(self, value):
        pass

    @property
    @abstractmethod
    def amplitude(self):
        """
        Get the amplitude of all cells

        Returns
        -------
        amplitudes: np.ndarray[float]
            amplitudes > 0, in this example they shall be ones
        """
        pass

    @amplitude.setter
    @abstractmethod
    def amplitude(self, value):
        pass

    @property
    def complex(self):
        """
        Get the amplitude and phase of all cells in complex representation

        Returns
        -------
        values: np.ndarray[np.complex]
            values = amplitudes * exp(1j * phases)
        """
        return self.amplitude * np.exp(1j * self.phase)

    @complex.setter
    def complex(self, value):
        self.amplitude[...] = np.abs(value)
        self.phase[...] = np.angle(value)

    def set_polar(self, amplitude, phase):
        self.amplitude[...] = amplitude
        self.phase[...] = phase

    @property
    def polar(self):
        """
        Get the amplitude and phase of all cells in polar representation

        Returns
        -------
        amplitudes: np.ndarray[float]
            amplitudes > 0, in this example they shall be ones
        phases: np.ndarray[float]
            phases in range [-pi, pi]
        """
        return self.amplitude, self.phase

    @polar.setter
    def polar(self, value):
        self.set_polar(*value)

    @property
    @abstractmethod
    def frequency(self):
        """
        Get the frequency of all cells

        Returns
        -------
        frequencies: np.ndarray[float]
            negative frequencies represent clockwise rotation
        """
        pass

    @frequency.setter
    @abstractmethod
    def frequency(self, value):
        pass

    def evolve(self, dt):
        """
        Do the time-evolution step of the oscillators

        Parameters
        ----------
        dt: float
            The time that has passed
        """
        dt = float(dt)
        w = 2 * np.pi * self.frequency
        self.phase = self.phase + dt * w  # note: += calls the __iadd__ on the result of the getter (that may be a copy)


class FloatOscillator(OscillatorBase):
    """
    A class that implements oscillator phase and amplitude through complex numbers
    """
    def __init__(self, shape):
        self._frequency = np.empty(shape, dtype=float)
        self._amplitude = np.empty(shape, dtype=float)
        self._phase = np.empty(shape, dtype=float)

    @property
    def phase(self):
        return self._phase

    @phase.setter
    def phase(self, value):
        self._phase[...] = value

    @property
    def amplitude(self):
        return self._amplitude

    @amplitude.setter
    def amplitude(self, value):
        self._amplitude[...] = value

    @property
    def frequency(self):
        return self._frequency

    @frequency.setter
    def frequency(self, value):
        self._frequency[...] = value


class ComplexOscillator(OscillatorBase):
    """
    A class that implements oscillator phase and amplitude through complex numbers
    """
    def __init__(self, shape):
        self._frequency = np.empty(shape, dtype=float)
        self._complex = np.empty(shape, dtype=np.complex)

    @property
    def phase(self):
        return np.angle(self._complex)

    @phase.setter
    def phase(self, value):
        self._complex[...] = np.abs(self._complex) * np.exp(1j * value)

    @property
    def amplitude(self):
        return np.abs(self._complex)

    @amplitude.setter
    def amplitude(self, value):
        self._complex[...] = np.abs(value) * np.exp(1j * np.angle(self._complex))

    @property
    def frequency(self):
        return self._frequency

    @frequency.setter
    def frequency(self, value):
        self._frequency[...] = value

    @property
    def complex(self):
        return self._complex

    @complex.setter
    def complex(self, value):
        self._complex[...] = value

    def set_polar(self, amplitude, phase):
        self._complex[...] = np.abs(amplitude) * np.exp(1j * phase)


class GridBase(ABC):
    """
    Implement a `m * m` grid of oscillator cells.
    The update rule is that the each node adjusts its neighbors to be closer to them
    (this is rather "opinion propagation" instead of "adaptation").
    There are several alternatives for update scheduling.

    Parameters
    ----------
    m: int
        side length of the grid
    interaction: (np.ndarray, np.ndarray, float) -> np.ndarray
        interaction between oscillators

    Attributes
    ----------
    d: int
        dimension of the grid
    coordinates_ : np.ndarray
        coordinates of the grid points, shape (m ** d, d)
    bulk_coordinates_ : np.ndarray
        coordinates of the non-border grid points, shape ((m-2) ** d, d)

    Properties
    ----------
    amplitude, phase, frequencyy, complex: np.ndarray
        amplitude, phase,frequency and state of the cells represented as a complex number exp(i*phase)
        shape (m, ) * d, e.g., (m, m); you can take a vector by .ravel() or the .flat iterator

    Notes
    -----
    You shall call `initialize` to set up the grid
    """

    def __init__(self, m, interaction):
        self.d = 2
        self.m = m
        self.interaction = interaction

    def initialize_cells(self, random_state=None):
        """
        Create grid of oscillators and set it up grid with random phases

        Parameters
        ----------
        random_state: np.random.RandomState | int
            Random state or seed
        """
        random_state = check_random_state(random_state)
        m, d = self.m, self.d
        shape = (m,) * d
        # Initialize grid with random phase
        phase = random_state.uniform(-np.pi, np.pi, shape)
        self.set_polar(1, phase)
        # Save the grid coordinates of each cell
        coordinates = np.asarray(list(np.ndindex(shape)))
        self.coordinates_ = coordinates
        self.bulk_coordinates_ = coordinates[np.all((0 < coordinates) & (coordinates + 1 < shape), axis=-1)]

    def neighbors_of(self, cell, exclude_self=True, boundary='clip'):
        """
        Calculate neighbors of a non-border cell within the grid.

        Parameters
        ----------
        cell: coordinates
            Coordinates of the origin, shape (n, d) or (d, )
        exclude_self: bool
            Whether ot exclude self-link

        Returns
        -------
        neighbors: list[coordinates]
            List of neighbors, shape (n, k, d)

        Notes
        -----
        The `steps` variable of this function could be cached for improved performance.
        This function can easily be replaced by one based on an adjacency matrix.
        """
        cell = np.asarray(cell)
        directions = (slice(-1, 2),) * self.d
        steps = np.stack(np.mgrid[directions], axis=-1)
        if exclude_self:
            steps = steps[np.any(steps, axis=-1)]
        else:
            steps = steps.reshape([-1, self.d])
        neighbors = cell[..., np.newaxis, :] + steps
        if boundary == 'wrap':
            neighbors = np.mod(neighbors, self.m)
        elif boundary == 'clip':
            if neighbors.ndim > 2:
                neighbors = [[n for n in nn if np.all((0 <= n) & (n < self.m))] for nn in neighbors]
            else:
                neighbors = [n for n in neighbors if np.all((0 <= n) & (n < self.m))]
        elif boundary == 'nanclip':
            valid = np.all((0 <= neighbors) & (neighbors < self.m), axis=-1)
            neighbors = neighbors.astype(float)
            neighbors[~valid] = np.nan
        return neighbors

    @property
    def _bulk_indexer(self):
        """
        Give an indexer to the non-border cells

        Returns
        -------
        indexer: tuple(slice, ...)
            tuple of slices
        """
        return (slice(1, self.m - 1),) * self.d

    @property
    def _bulk_coordinates(self):
        """
        Give an indexer to the non-border cells

        Returns
        -------
        indexer: np.ndarray
            list of coordinates
        """
        projection1d = (slice(0, self.m-1),) * self.d
        return np.mgrid(projection1d)

    @property
    def size(self):
        """
        Return the size of the grid (number of cells)

        Returns
        -------
        n: int
            number of cells in the grid
        """
        return self.complex.size

    def _update_cells(self, preferred, cells):
        """
        Update selected cells to be closer to the preferred angle

        Parameters
        ----------
        preferred: np.complex
            Preferred angle as a unit-length complex number
        cells: list_like[coordinates]
            List of coordinates of affected points, shape (n_points, d)
        """
        indexer = tuple(np.asarray(cells).T)
        original = self.phase[indexer]
        updated = self.interaction(original[:, np.newaxis], preferred, self.learning_rate, axis=-1, discard_nan=True)
        self.phase[indexer] = updated[:, 0]

    def deprecated_parallel_update(self):
        """
        Update all cell phases at the same time (scheduling rule "parallel")

        Notes
        -----
        This method is efficient (fast) due to using numpy/scipy functions instead of python loops.
        It is also easy to implement on a GPU.
        """
        # TODO: a version that uses parallel neighbor lookup and _update_cells instead of convolution and direct update
        from scipy.signal import convolve
        m, d = self.m, self.d
        # Prepare weights how the central cell affects its neighbors
        # This can be seen as a slice of the adjacency matrix
        weights = np.full((3,) * d, self.learning_rate, dtype=float)
        weights[(1,) * d] = 0
        # Represent phase as complex number, see explanation at top
        cells = self.complex
        # Similarly to a sparse matrix multiplication with the adjacency matrix
        # here, new phase is expressed as an n-dimensional convolution.
        # - using "bulk", we discard the border cells from the input as requested in the task description
        # - mode='full' expands results over boundaries
        # - method='direct' is safe (exact) due to the small number of element (note: 'fft' can be faster)
        preferred = convolve(cells[self._bulk_indexer], weights, mode='full', method='direct')
        # the resulting values have correct phase but amplitude may vary, we extract the phase
        updated = to_unit_circle(cells + preferred)
        self.complex = updated

    def parallel_update(self):
        """
        Update all cell phases at the same time (scheduling rule "parallel")

        Notes
        -----
        This method is efficient (fast) due to using numpy/scipy functions instead of python loops.
        It is also easy to implement on a GPU.
        """
        m, d = self.m, self.d
        # Represent phase as complex number, see explanation at top
        cells = self.coordinates_
        # Similarly to a sparse matrix multiplication with the adjacency matrix
        # here, new phase is expressed as an n-dimensional convolution.
        # - using "bulk", we discard the border cells from the input as requested in the task description
        # - mode='full' expands results over boundaries
        # - method='direct' is safe (exact) due to the small number of element (note: 'fft' can be faster)
        preferred = nan_indexing(self.phase, *np.transpose(self.neighbors_of(cells, boundary='nanclip'), (-1, 0, 1)))
        # the resulting values have correct phase but amplitude may vary, we extract the phase
        self._update_cells(preferred, cells)

    def permutation_update(self, random_state=None):
        """
        Update all cell phases in a random order (scheduling rule "permutation")

        Parameters
        ----------
        random_state: np.random.RandomState | int
            Random state or seed

        Notes
        -----
        Due to the random accessing order of the cells this algorithm is less suitable for a GPU.
        """
        random_state = check_random_state(random_state)
        permutation = random_state.permutation(self.bulk_coordinates_)
        for c in permutation:
            self._update_cells(self.phase[tuple(c)], self.neighbors_of(c))

    def random_update(self, n=None, random_state=None):
        """
        Update phase based on `n` random cells (scheduling rule "random")

        Parameters
        ----------
        n: int
            How many updates to perform, default: None meaning as many as the number of cells
            but repetition is allowed
        random_state: np.random.RandomState | int
            Random state or seed

        Notes
        -----
        Due to the random accessing order of the cells this algorithm is less suitable for a GPU.
        """
        random_state = check_random_state(random_state)
        if n is None:
            n = len(self.bulk_coordinates_)
        choices = random_state.choice(len(self.bulk_coordinates_), size=n, replace=True)
        active = self.bulk_coordinates_[choices]
        for c in active:
            self._update_cells(self.phase[tuple(c)], self.neighbors_of(c))


class Grid(GridBase, FloatOscillator):
    """
    Implement a `m * m` grid of oscillators cells that have:
    - the same frequency,
    - the same amplitude,
    - variable phase.
    The update rule is that the each node adjusts its neighbors to be closer to them
    (this is rather "opinion propagation" instead of "adaptation").
    There are several alternatives for update scheduling.

    Parameters
    ----------
    m: int
        side length of the grid
    frequency: float
        frequency of oscillation
    learning_rate: float
        speed of adaptation applied on neighbors

    Attributes
    ----------
    d: int
        dimension of the grid
    coordinates_ : np.ndarray
        coordinates of the grid points, shape (m, ) * d + (d, ), e.g., (m, m, d)
    bulk_coordinates_ : np.ndarray
        coordinates of the non-border grid points, shape (m-2, ) * d + (d, ), e.g., (m-2, m-2, d)

    Properties
    ----------
    complex: np.ndarray
        phase of the cells represented as a complex number exp(i*phase) while amplitude==1 is maintained;
        shape (m, ) * d, e.g., (m, m)
    phase: np.ndarray
        phase of the cells

    Notes
    -----
    You shall call `initialize` to set up the grid
    """

    def __init__(self, m, frequency, learning_rate, interaction=None):
        if interaction is None:
            interaction = PhaseInteraction(potential=1, symmetry=1, representation='float')
        GridBase.__init__(self, m=m, interaction=interaction)
        FloatOscillator.__init__(self, shape=(m, m))
        self.frequency = frequency
        self.learning_rate = learning_rate


def nan_indexing(arr, *indices):
    isnan = np.any(np.isnan(indices), axis=0)
    allowed = tuple(np.nan_to_num(i).astype(int) for i in indices)
    result = arr[allowed]
    if np.any(isnan):
        result[isnan] = np.nan
    return result


def simulate(my_grid, dt=0.002, n=400, schedule='parallel', random_state=None):
    """
    Run the simulation

    Parameters
    ----------
    my_grid: Grid
        A Grid instance, see parameters there
    dt: float
        Time step of the simulation
    n: int
        Number of time steps in the simulation
    schedule: str
        Schedule for updating phase with respect to neighbors.
        Must be one of: 'parallel', 'permutation', 'random'.
    random_state: np.random.RandomState | int
        Random state or seed
    """
    dt = float(dt)
    n = int(n)
    random_state = check_random_state(random_state)

    my_grid.initialize_cells(random_state=random_state)
    coordinates = my_grid.coordinates_
    phases = np.empty((n, my_grid.size), dtype=float)
    t = np.arange(n) * dt

    for i in range(n):
        phases[i] = my_grid.phase.flat
        if schedule == 'oldparallel':
            my_grid.deprecated_parallel_update()
        elif schedule == 'parallel':
            my_grid.parallel_update()
        elif schedule == 'permutation':
            my_grid.permutation_update(random_state=random_state)
        elif schedule == 'random':
            my_grid.random_update(n=None, random_state=random_state)
        else:
            raise ValueError('Invalid update rule')
        my_grid.evolve(dt)

    return {'coordinates': coordinates, 'time': t, 'phases': phases}
